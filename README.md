**Глоссарий**

***Владелец(owner, O)*** - peer, владелец криптокошелька и средств.

***Злоумышленник(robber, R)*** - peer, вымогатель, пытающийся с помощью угроз и насилия в реальной жизни вынудить владельца сделать перевод средств с кошелька владельца на кошелек злоумышленника.

***Защитник(defender, D)*** - peer, которому доверяет владелец и которого желает уведомить владелец о действиях злоумышленника.

***Подпись(S)*** - электронная подпись.

***Аварийный секретный ключ(emergency secret key, ESK)*** - ключ, которым владелец подписывает транзакцию, если считает, что имеет место вымогательство со стороны злоумышленника.

***Аварийный членский ключ(emergency member key, EMK)*** - ключ, которым D подтверждает что транзакция подписана ESK.

***Основной закрытый ключ(Primary private key, PPK)*** - ключ, которым владелец подписывает транзакцию во всех остальных случаях.

***Основной членский ключ(primary member key, PMK)*** - ключ, которым D подтверждает что транзакция подписана PPK.


***Администратор, админ(admin, A)*** - peer, способный отличить подпись ESK от PPK.

***Открытый ключ владельца(owner public key, OPK)*** - идентификатор, адрес владельца, кошелек, аналог публичного ключа в биткойн. В случае с групповой подписью это групповой публичный ключ.

***Секретный ключ админа(admin secret key, ASK)*** - ключ, которым A расшифровывает подпись O и отличает подпись ESK от OPK.

***Транзакция(transaction, T)*** - транзакция.

***Транзакция отмены(cancel transaction, CT)*** - транзакция, которая доказывает, что транзакция, на которую она ссылается была подписана ESK.

Задача проекта **уведомить** админа о возможном вымогательстве средств у владельца криптокошелька. Владельца криптокошелька сообщает злоумышленнику аварийный секретный ключ, а не основной секретный ключ, но злоумышленник не может отличить один от другого. Для всех пользователей сети транзакция подписанная любым из этих ключей выглядит одинаково валидно. Админ единственный пользователь кто может вскрыть подпись и отличить каким ключом была подписана транзакция. После вскрытия админ отправляет в сеть подтверждение, которое может верифицировать любой пользователь сети. Все транзакции, которые сделаны на основе транзакции, подписанной Аварийным секретным ключом не действительны.


В идеале хотелось бы, чтобы у O было несколько A, чтобы исключить возможность вымогательства средств со стороны A (случай, когда A == R).


Функционал подписи двумя разными ключами, подписи которыми невозможно отличить для всех peer'ов кроме админа можно получить с помощью [групповой подписи](https://en.wikipedia.org/wiki/Group_signature). Сейчас нужно выбрать схему, которая может быть использована в варианте децентрализованного A. [Статья со ссылками на схемы подписей](https://www.researchgate.net/publication/284819226_Group_Signatures_in_Practice)

**Список статей со схемами подписи**
1. [Group Signatures in Practice](https://www.researchgate.net/publication/284819226_Group_Signatures_in_Practice)
    - не подходит для DA и CA, нет GPK. Содержит список схем.
2. [Efficient group signature schemes for large groups](https://link.springer.com/chapter/10.1007/BFb0052252)
    - подходит для CA.
3. [Separability and Efficiency for Generic Group Signature Schemes](https://www.semanticscholar.org/paper/Separability-and-Efficiency-for-Generic-Group-Camenisch-Michels/9be7a72a2839237d355b51162c7b8f8fc2dc6b47)
    - ничего не понятно
4. [Efficient Revocation in Group Signatures](https://www.researchgate.net/publication/2554976_Efficient_Revocation_in_Group_Signatures)
    - то же самое, что в 2, но с возможностью отзыва члена из группы. Не разбирался как работает отзыв, т.к. это не обязательно
5. [A STUDY ON EFFICIENT GROUP-ORIENTED SIGNATURE
SCHEMES FOR REALISTIC APPLICATION ENVIRONMENT](https://www.researchgate.net/publication/2554976_Efficient_Revocation_in_Group_Signatures)
    - схемы нет.
6. [Ogawa, K., Ohtake, G., Fujii, A., Hanaoka, G.: Weakened anonymity of group signature
and its application to subscription services](https://www.researchgate.net/publication/270441383_Weakened_Anonymity_of_Group_Signature_and_Its_Application_to_Subscription_Services)
    - бесплатной статьи нет
7. [An r-Hiding Revocable Group Signature Scheme: Group Signatures with the Property of Hiding the Number of Revoked Users](https://www.researchgate.net/publication/263443894_An_r-Hiding_Revocable_Group_Signature_Scheme_Group_Signatures_with_the_Property_of_Hiding_the_Number_of_Revoked_Users)
    - в основном про отзыв.
8. [A Practical and Provably Secure Coalition-Resistant Group Signature Scheme](https://www.researchgate.net/publication/221354922_A_Practical_and_Provably_Secure_Coalition-Resistant_Group_Signature_Scheme)
9. [David Pointcheval and Olivier Sanders: Short Randomizable Signatures](https://eprint.iacr.org/2015/525)
    - то же, что и 2, но есть возможность для Signature Proof of Knowledge (возможности открыть подпись не раскрывая секретных ключей членов группы). Наилучший вариант на данный момент.

**Требования к подписи для варианта децентрализованного A**
- подпись ESK и PPK может различить только admin
- наличие APK

**Требования к CT**
- доказать что открытая подпись соответсвует закрытой подписи, загруженной ранее в блокчейн.
- доказать, каким ключом была сделана подпись без раскрытия EMK и PMK.
- доказать, что транзакцию раскрыл пользователь, обладающий EMK, PMK, ASK.

Вариант 9 отвечает требованиям к CT, т.к. имеет Signature Proof of Knowledge(SPK).

**Проблемы с вариантом 9**
- может ли O 

**Алгоритм для варианта 9**
- O подписывает T.
- A открывает подпись и выкладывает сообщение содержащее:
    - ссылку на транзакцию, которая содержит подпись.
    - 0 если EMK, 1 если PMK.
    - SPK.
- T доверяют или нет, после подтверждения A.

**Проблемы**
- не понятно как должна выглядеть транзакция, отменяющаю транзакцию, подписанную ESK без открытия каких-либо секретов.
    - Вариант с выкладыванием расшифованной ASK подписи(далее ASK S):
        - ***Вопросы***:
            - если выложить ASK S, откроет ли это ASK?
            - как доказать что эта расшифрованная ASK подпись?
                - как вариант нужен открытый ключ админа.
            - откроет ли это ESK?
                - не должно, но нужно также опубликовать EMK, что не хорошо.
            - можно в эту транзакцию добавить новый адрес, т.к. если старый будет скомпроментирован транзакцией отмены
        - ***Алгоритм***
            - проверить, что ASK S это расшифрованная админом S
            - проверить, что CT подписана  


# **Вариант централизованного админа(то на что сейчас хватает знаний)**
## **Глоссарий для варианта СA**
*Открытый ключ группы(Group public key, GPK)*
*Закрытый ключ группы(Group secret key, GSK)*
## **Camenisch, J., Stadler, M.: Efficient group signature schemes for large groups.Lecture Notes in Computer Science 1296 (1997) 410–424** 
- В этой схеме D и A это один peer(одно и то же).
- D создает пару ключей GPK и GSK. GPK имеет роль OPK для O, GSK - ASK.
- O создает две пары ключей (EMK,ESK) и (PMK,PPK), вносит их в группу отправляя EMK и PMK D(возможно скрытно, в обход блокчейна, возможно открыто).
- O оставляет в блокчейне запись о том, что все транзакции O должны быть направлены на подпись D перед отправкой в блокчейн. Так D может узнать, что T подписана ESK и имеет место аморальный грабеж O. 
- Теперь все исходящие транзакции от O должны быть отправлены на подпись D перед встройкой в блокчейн. Только D может понять каким ключом была подписана транзакция. Для всех остальных разницы нет.
**Проблемы**
- только один D на O.
- проблемы ниже можно решить если установить глубину подписи(T от O проверяет D1, T от D1 проверяет его D2, и.т.д. Но это увеличивает сложность транзакций и если можно будет устанавливать глубину R всегда будет ставить 1)
    - сговор D и R.
    - атака R на D перед атакой на O.
- сговор D и O чтобы обмануть peer'а не R(у O никто не вымогает, он решил обмануть другого peer сговорившись с D)(решается лимитом на отмену транзакции в 24 часа, после этого срока транзакцию отменить нельзя, даже если она была подписана ESK) 
**Вопросы**
- EMK и PMK можно хранить открыто, т.к. для вскрытия подписи все равно требуется ASK?
- разделение ролей 
- как доказать криптографически что D имеет ASK, связять его с OPK D.
- зачем нужны member certificate
- `An obvious (and for the second scheme simple) extension would be to assign the
different roles of the group manager to different entities, i.e., to a membership
manager, who is responsible for adding new members to the group, and to a
revocation manager, who is responsible for opening signatures. The functionality
of these managers can also be shared among several entities. The realization is
straightforward.`

# **Вариант децентрализованного админа(DA)(вариант максимум)**
## **Глоссарий для варианта DA**
**Аварийный групповой ключ(Emergency member key, EMK)** - ключ, пара к ESK, но его связь с OPK и ESK не должна быть установлена. Используется A для различения подписи ESK от PPK. 
**Открытый ключ админа(admin public key, APK)** - ключ, идентификатор админа. Идея его использования в том, что peer, который смог подобрать закрытый ключ к APK становится децентрализованным A и может посмотреть кем подписаны все текущие транзакции. После этого он строит новый блок, и уведомляет всех D для текущих транзакции о том грабят ли O. Пока подпись с таким примитивом не найдена(возможно такой и нет).

**Тезисы**
- A должен представлять из себя пару ключей(APK, ASK)
- O должен представлять из себя ключи(OPK, PPK, ESK, EKID)
- S = f(T, APK, PPK или ESK)(что должна принимать функция создания подписи)
- Должен существовать сложный алгоритм(майнинг) получения из APK->ASK
- Должна быть функция генерации APK, например от текущего времени или от хэша предыдущего блока

**Алгоритм генерации блока при децентрализованном админе**
- Каждые n секунд в сети меняется APK(информация открытая, генерируется по алгоритму)
- Все транзакции, которые желают быть в следующем блоке должны быть подписаны с помощью этого APK
- Майнеры начинают подбирать ASK, чтобы иметь возможность создать следующий блок
- Параллельно идет процесс формирования вспомогательного блока с помощью какого-либо механизма консенсуса, куда собираются транзакции подписанные текущим APK
- Peer, который нашел ASK из APK создает следующий блок в котором будут содержаться транзакции подписанные APK 

**Черновик**
*Закрытый ключ защитника(defender secret key, DSK)*
*Открытый ключ защитника(defender public key, DPK)*
Может хранится открыто в блокчейне как связь DPK-EMK для установления какого D стоит уведомлять при подписи T конкретным ESK. При раскрытии связи с PPK будет установлена связь OPK-DPK, что может привести к физической атаке на D. Может быть хранить в секрете связь OPK-DPK не обязательно.

**Проблемы**
- DA после вскрытия ASK может выложить в обход блокчейна информацию о том аварийные ли транзакции были в блоке, тем самым подставив всех O в опасности. Защититься от этого можно только защищенным кодом, который невозможно вскрыть и увидеть ASK(основной минус схемы DA)
- 